package id.go.palembang.hallo.monitoringpajak;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;
import android.app.Activity;
import id.go.palembang.hallo.monitoringpajak.MainActivity;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

/**
 * Created by Indra Maulana on 22/06/2019.
 */

public class WebViewJavascriptInterface extends AppCompatActivity {
    Context mContext;
    String webUrl;

    /** Instantiate the interface and set the context */
    WebViewJavascriptInterface(Context c) {
        mContext = c;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public void pajak(){
        Log.d("JS", "Launching Another Activity:");
        Activity activity = new Activity();
        Intent i = new Intent(mContext, LandscapeBrowser.class);
        String message = "http://103.138.143.19/monitoring/Menu_Pajak/iframe/13";
        i.putExtra("Link", message);
        mContext.startActivity(i);
    }

    @JavascriptInterface
    public void menara(){
        Log.d("JS", "Launching Another Activity:");
        Activity activity = new Activity();
        Intent i = new Intent(mContext, LandscapeBrowser.class);
        String message = "http://103.138.143.19/monitoring/Menu_Retribusi/iframe/2";
        i.putExtra("Link", message);
        mContext.startActivity(i);
    }

    @JavascriptInterface
    public void menu(int cek){
        if(cek == 1){

        }

        else if (cek == 2) {
            Log.d("JS", "Launching Another Activity:");
            Activity activity = new Activity();
            Intent i = new Intent(mContext, LandscapeBrowser.class);
            String message = "http://103.138.143.19/monitoring";
            i.putExtra("Link", message);
            mContext.startActivity(i);
        }
    }

    public void putar() {
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
    }

}
